/*
 * org.nrg.xft.layeredSequence.LayeredSequenceObject
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/1/13 9:13 AM
 */


package org.nrg.xft.layeredSequence;

import org.nrg.xft.ItemI;


/**
 * @author Tim
 *
 */
public abstract class LayeredSequenceObject implements LayeredSequenceObjectI,ItemI{
}
